package ftr_old;
import java.util.PriorityQueue;

import ftr_old.board.BlockNode;
import ftr_old.datastructures.BinaryHeap;
import ftr_old.datastructures.Heuristic;
import ftr_old.tree.StateNode;

public class HeapTester {

	public static void main(String[] args) {
		BlockNode[][] board = BlockNode.deserializeBoard(1);
		// BlockNode.printBoard(board);
		PriorityQueue<StateNode> openSet = new PriorityQueue<StateNode>(11, (n1, n2) ->{
			if(n1.getCost() + n1.getHeuScore() < n2.getCost() + n2.getHeuScore()){
				return -1;
			}
			else if(n1.getCost() + n1.getHeuScore() > n2.getCost() + n2.getHeuScore()){
				return 1;
			}
			else{
				return 0;
			}
		});
		
		Heuristic<StateNode,BlockNode> manhattanDistance = (node, goal, localboard) -> {
			
			int nodeX = node.getState().getX();
			int nodeY = node.getState().getY();
			int goalX = goal.getX();
			int goalY = goal.getY();
			return Math.abs(nodeX - goalX) + Math.abs(nodeY - goalY);
		};

		BlockNode goal = board[0][4];

		BinaryHeap<StateNode,BlockNode> heap = new BinaryHeap<StateNode,BlockNode>(manhattanDistance, goal, board);

		StateNode temp = new StateNode(board[0][0]); // manhattanDistance = 2
		StateNode temp2 = new StateNode(board[0][0]); // manhattanDistance = 2
		StateNode temp3 = new StateNode(board[0][0]); // manhattanDistance = 2
		temp.setCost(0);
		temp2.setCost(1);
		temp3.setCost(3);
		temp.calcHeuristic(manhattanDistance, goal, board);
		temp2.calcHeuristic(manhattanDistance, goal, board);
		temp3.calcHeuristic(manhattanDistance, goal, board);
		openSet.add(temp);
		openSet.add(temp2);
		openSet.add(temp3);
		System.out.println(openSet.poll());
		System.out.println(openSet.poll());
		System.out.println(openSet.poll());


		
	}

}
