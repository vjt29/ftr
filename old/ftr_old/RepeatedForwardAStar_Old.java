package ftr_old;

import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;

import ftr.board.BlockNode;
import ftr.board.Board;
import ftr.tree.StateNode;

public class RepeatedForwardAStar_Old {
	
	public StateNode computerPath(){
		return null;
	}
	
	public static void main(String[] args){
		Board board = new Board(50);
		Board agentBoard = new Board(board.board.length, board.board.length);
		agentBoard.setGoal(100, 98);
		agentBoard.setStart(5, 0);
		board.setGoal(100, 98);
		board.setStart(5, 0);
		int counter = 1;
		PriorityQueue<StateNode> openSet = new PriorityQueue<StateNode>((sn1, sn2) ->{
			sn1.recomputeFValue();
			sn2.recomputeFValue();
			if(sn1.f > sn2.f){
				return 1;
			}
			else if(sn1.f < sn2.f){
				return -1;
			}
			else{
				if(sn1.g > sn2.g){
					return 1;
				}
				else if(sn1.g < sn2.g){
					return -1;
				}
				else{
					return 0;
				}
			}
			
		});
		HashSet<StateNode> closedSet = new HashSet<StateNode>();
		StateNode start = new StateNode(board.board[5][0],agentBoard,null,counter,0,agentBoard.manhattanDistance(board.board[0][0]));
		StateNode goal = new StateNode(board.board[100][98],agentBoard,null,counter,Integer.MAX_VALUE,agentBoard.manhattanDistance(board.board[4][4]));
		openSet.add(start);
		StateNode tree = start;
		StateNode[][] encounteredStates = new StateNode[board.board.length][board.board[0].length];
		while(true){
			
			StateNode currState = openSet.poll();
			if(currState.equals(goal)){
				System.out.println("goal");
				goal = currState;
				break;
			}
			closedSet.add(currState);
			BlockNode[] neighbors = currState.getOpenNeighbors();
			for(BlockNode b: neighbors){
				StateNode temp = null;
				if(encounteredStates[b.x][b.y] != null){
					temp = encounteredStates[b.x][b.y];
				}
				else{
					temp = new StateNode(agentBoard.board[b.x][b.y],agentBoard, currState, 0, currState.g+1, agentBoard.manhattanDistance(b));
					encounteredStates[temp.bn.x][temp.bn.y] = temp;
				}
				if(temp.search < counter){
					temp.g = Integer.MAX_VALUE;
					temp.search = counter;
					temp.recomputeFValue();
				}
				if(temp.g > currState.g + 1){
					temp.g = currState.g + 1;
					temp.recomputeFValue();
					temp.parent = currState;
					openSet.remove(temp);
					openSet.add(temp);
				}
			}
			
		}
		StateNode temp = goal;
		while(temp != null){
			System.out.println(temp.bn);
			temp = temp.parent;
		}
		
		
	}
}
