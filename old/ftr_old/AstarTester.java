package ftr_old;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;

import ftr_old.board.BlockNode;
import ftr_old.datastructures.BinaryHeap;
import ftr_old.datastructures.Heuristic;
import ftr_old.tree.StateNode;

public class AstarTester {
	
	public static StateNode computePath(Heuristic<StateNode,BlockNode> formula, StateNode[][] board,BlockNode[][] board2,StateNode goalState,int counter, PriorityQueue<StateNode> openSet, LinkedList<StateNode> closedSet){
		StateNode path = null;
		int test = 0;
		while(openSet.peek() != null && goalState.getCost() > openSet.peek().getCost()){
			
			StateNode curr = openSet.poll();
			if(curr.equals(goalState)){
				curr.setCost(path.getCost() +1);
				if(curr.equals(path) == false){
					curr.setParent(path);
				}
				return curr;
			}
			closedSet.add(curr);
			//calculate actions
			int currX = curr.getState().getX();
			int currY = curr.getState().getY();
			ArrayList<StateNode> neighbors = new ArrayList<StateNode>();
			if(currX > 0){ //add left if left in bounds
					neighbors.add(board[currX-1][currY]);
			}
			if(currX < board.length-1 ){ //add right if right in bounds
					neighbors.add(board[currX+1][currY]);
			}
			if(currY > 0){//add below if below in bounds
					neighbors.add(board[currX][currY-1]);
			}
			if(currY < board[currX].length-1){//add up if up in bounds
					neighbors.add(board[currX][currY+1]);
			}
			for(StateNode s: neighbors){
				if(closedSet.contains(s)){
					continue;
				}
				if(s.getState().getType() == BlockNode.WALL){
					s.setCost(Integer.MAX_VALUE);
					continue;
				}
				if(s.searchVal < counter){
					s.setCost(Integer.MAX_VALUE);
					s.searchVal = counter;
				}
				if(s.getCost() > curr.getCost() + 1){
					s.setCost(curr.getCost()+1);
					s.calcHeuristic(formula, goalState.getState(), board2);
					path = s;
					s.setParent(curr);
					openSet.add(s);
				}
			}
			System.out.println(openSet.toString());
		}
		return path;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BlockNode[][] board = BlockNode.deserializeBoard(2);
		BlockNode.printBoard(board);
		StateNode[][] stateBoard = new StateNode[board.length][board[0].length];
		for(int i = 0;i < board.length;i++){
			for(int j = 0;j < board.length;j++){
				stateBoard[i][j] = new StateNode(board[i][j], Integer.MAX_VALUE);
			}
		}
		LinkedList<StateNode> closedSet = new LinkedList<StateNode>();
		BlockNode goal = board[4][0];
		BlockNode start = board[0][0];
		StateNode startState = new StateNode(start, 0);
		StateNode goalState = new StateNode(goal, Integer.MAX_VALUE); //goal state with infinity as cost
		stateBoard[4][4] = goalState;
		Heuristic<StateNode,BlockNode> formula  = (node, localgoal, localboard) -> { //formula = manhatten distance + node cost
			int nodeX = node.getState().getX();
			int nodeY = node.getState().getY();
			int goalX = localgoal.getX();
			int goalY = localgoal.getY();
			double manhattenDist =  Math.abs(nodeX - goalX) + Math.abs(nodeY - goalY);
			//System.out.println(node.getCost() + "," + manhattenDist);
			return (int) (manhattenDist + node.getCost()); //return h(s)+g(s)
		};
		PriorityQueue<StateNode> openSet = new PriorityQueue<StateNode>(11, (n1, n2) ->{
			if(n1.getCost() + n1.getHeuScore() < n2.getCost() + n2.getHeuScore()){
				return -1;
			}
			else if(n1.getCost() + n1.getHeuScore() > n2.getCost() + n2.getHeuScore()){
				return 1;
			}
			else{
				return 0;
			}
		});
		openSet.add(startState);
		
		//start computepath
		StateNode path = computePath(formula, stateBoard, board,goalState, 0,openSet,closedSet);
		StateNode temp = path;
		while(temp != null){
			System.out.println(temp + " x:" + temp.getState().getX() + ", y: " + temp.getState().getY() );
			temp = temp.getParent();
		}
		System.out.println(openSet);
		
	}

}
