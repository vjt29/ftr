package ftr_old.datastructures;

/* library imports */
import java.util.ArrayList;
import java.util.Collections;

import ftr_old.datastructures.Heuristic;

/**
 * @author Kyle
 * 
 *         A binary implementation of a minheap, allowing for a user-defined
 *         heuristic. The higher the priority, the lower the value of the
 *         heuristic
 * 
 * @param <T>
 *            the data structure that the heap is prioritizing with the
 *            Heuristic h
 */
public class BinaryHeap<S,T> {
	
	// TODO Test for proper functionality

	// class variables
	private Heuristic<S,T> h;
	private ArrayList<S> heap;
	private int size;

	private T goal;
	private T[][] board;

	/* INITIALIZATION */
	public BinaryHeap(Heuristic<S,T> h, T goal, T[][] board) {
		this.heap = new ArrayList<>();
		this.goal = goal;
		this.board = board;
		this.h = h;
		this.size = 0;
	}

	/* HEAP INTERACTION */

	/**
	 * @return the first element with highest priority in the heap
	 * @throws IllegalStateException()
	 *             if the heap is empty
	 */
	public S peek() {
		// error checking
		if (size == 0)
			return null;

		// returning the first element in the heap
		return heap.get(0);
	}

	/**
	 * removes the highest priority element from the array
	 * 
	 * @return the highest priority element from the heap
	 */
	public S poll() {
		// error checking
		if (size == 0)
			throw new IllegalStateException();

		// removing the item from the heap
		S item = heap.get(0);
		heap.remove(0);
		if(heap.size() > 1){ //if there's other elements besides the root in the heap
		// re-heapifying
			heap.set(0, heap.get(size - 1));
		}
		size--;
		heapifyDown();

		return item;
	}

	/**
	 * adds an item to the heap and re-heapifies
	 * 
	 * @param item
	 *            the item to be added
	 */
	public void add(S item) {
		// adding to the end of the heap
		heap.add(size, item);

		// re-heapifying
		size++;
		heapifyUp();
	}

	/* HEAPIFICATION */
	/**
	 * Starts at the top and walks downwards
	 */
	private void heapifyDown() {
		int index = 0;
		while(hasLeftChild(index)){
			// selecting the smallest index among the children
			int smallerChildIndex = getLeftChildIndex(index);
			if(hasRightChild(index) && h.eval(rightChild(index), goal, board) < h.eval(leftChild(index), goal, board)){
				smallerChildIndex = getRightChildIndex(index);
			}
			
			// checking if the heap is in order
			if(h.eval(heap.get(index), goal, board) < h.eval(heap.get(smallerChildIndex), goal, board)){
				break;
			}
			
			// swapping elements and walking downwards
			Collections.swap(heap, index, smallerChildIndex);
			index = smallerChildIndex;
		}
	}

	/**
	 * Starts at the bottom and walks upwards
	 */
	private void heapifyUp() {
		int index = size - 1;
		// loop while the parent is greater than the child
		while (hasParent(index) && h.eval(parent(index), goal, board) > h.eval(heap.get(index), goal, board)) {
			// swap elements
			Collections.swap(heap, getParentIndex(index), index);
			// walk upwards
			index = getParentIndex(index);
		}
	}

	/* HELPER METHODS */
	private int getLeftChildIndex(int parentIndex) {
		return 2 * parentIndex + 1;
	}

	private int getRightChildIndex(int parentIndex) {
		return 2 * parentIndex + 2;
	}

	private int getParentIndex(int childIndex) {
		return (childIndex - 1) / 2;
	}

	private boolean hasLeftChild(int index) {
		return getLeftChildIndex(index) < size;
	}

	private boolean hasRightChild(int index) {
		return getRightChildIndex(index) < size;
	}

	private boolean hasParent(int index) {
		return getParentIndex(index) >= 0;
	}

	private S leftChild(int index) {
		return heap.get(getLeftChildIndex(index));
	}

	private S rightChild(int index) {
		return heap.get(getRightChildIndex(index));
	}

	private S parent(int index) {
		return heap.get(getParentIndex(index));
	}
	
	/* PRINTING */
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for(S s : heap){
			sb.append(s.toString());
		}
		return sb.toString();
	}

}
