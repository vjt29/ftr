package ftr_old.datastructures;

/**
 * The Heuristic is a lambda expression that is used in the binary heap to prioritize which nodes will be visited next.
 * @param <state> the state being stored in the heap
 * @param <goal> the goal state of the board
 * @param <board> the board containing <node> and <goal>
 * @return a numeric measure of the expected cost from the current node to a goal state on a given board
 */
public interface Heuristic<S,T> {

	public double eval(S state, T goal, T[][] board);
	
}
