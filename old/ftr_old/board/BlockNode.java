package ftr_old.board;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Stack;
import java.util.stream.Stream;

public class BlockNode {

	public static final int OPEN = 0;
	public static final int WALL = 1;
	public static final int GOAL = 2;
	
	private int type; //type of board piece, 0 = normal, 1 = wall, 2 = goal
	private int x;
	private int y;
	
	public BlockNode(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public BlockNode(int x, int y, char type){
		this.x = x;
		this.y = y;
		if(type == 'G'){ //if goal
			this.type = GOAL;
		}
		else if(type == 'X'){//if wall
			this.type = WALL;
		}
		else{ //if neither, open space
			this.type = OPEN;
		}
	}

	/**
	 * Changes the type of block to a different type
	 * @param type the new type to change to
	 */
	public void changeType(int type){
		this.type = type;
	}
	
	

	
	
	public static BlockNode[][] generateBoard(int x, int y){ //generates a board with dimensions x and y
		BlockNode[][] board = new BlockNode[x][y]; //the actual board
		boolean[][] visited = new boolean[x][y]; //a visited matrix
		int visitednum = 0; //the number of visited blocks
		Stack<BlockNode> stack = new Stack<BlockNode>(); //the stack used for dfs
		
		for(int i = 0;i < x;i++){ //generate a new board
			for(int j = 0;j < y;j++){
				board[i][j] = new BlockNode(i,j);
			}
		}
		
		Random rng = new Random(); //randomly generate a starting position
		int startx = rng.nextInt(x);
		int starty = rng.nextInt(y);
		stack.add(board[startx][starty]);
		while(visitednum < x*y){
			while(!(stack.isEmpty())){
				BlockNode curr = stack.pop();
				int currX = curr.getX();
				int currY = curr.getY();
				if(!(visited[currX][currY])){
					visited[currX][currY] = true;
					visitednum++;	
				}
				ArrayList<BlockNode> neighbors = new ArrayList<BlockNode>();
				//populate neighbor list with non-visited neighbors
				if(currX > 0){ //add left if left in bounds
					if(visited[currX-1][currY] == false){
						neighbors.add(board[currX-1][currY]);
					}
				}
				if(currX < x-1 ){ //add right if right in bounds
					if(visited[currX+1][currY] == false){
						neighbors.add(board[currX+1][currY]);
					}
				}
				if(currY > 0){//add below if below in bounds
					if(visited[currX][currY-1] == false){
						neighbors.add(board[currX][currY-1]);
					}
				}
				if(currY < y-1){//add up if up in bounds
					if(visited[currX][currY+1] == false){
						neighbors.add(board[currX][currY+1]);
					}
				}
				
				if(neighbors.size() > 0){ //pick a random neighbor and either block it, or add at it to the stack
					int neighborRNG = rng.nextInt(neighbors.size());
					BlockNode neighbor = neighbors.get(neighborRNG);
					int neighborX = neighbor.getX();
					int neighborY = neighbor.getY();
					visited[neighborX][neighborY] = true;
					visitednum++;
					Double blockChance = rng.nextDouble();
					if(blockChance <= .3){
						neighbor.changeType(WALL);
						stack.add(curr);
					}
					else{
						stack.add(curr); //add the parent again before the neighbor to backtrack if any neighbors end up unvisited
						stack.add(neighbor); //add the neighbor
					}
				}
				
			}
			
			for(int i = 0; i < x;i++){ //if the stack is empty and not all blocks have been traversed, find the first node not touched and add it to the stack, then break, else all nodes touched
				for(int j = 0;j < y;j++){
					if(visited[i][j] == false){
						stack.add(board[i][j]);
						break;
					}
				}
			}
			
		}
		
		return board;
	}
	
	/**
	 * Opens a .txt file, parses it, and converts it to objects that can be used as a game board.
	 * @param world the world to be deserialized, only numbers which correspond to a previously generated world in the resources folder are valid 
	 * @return a 2d array made up of blockNodes, generated from the .txt file opened
	 */
	public static BlockNode[][] deserializeBoard(int world){
		String prefix = "resources/generated_world";
		String suffix = ".txt";
		String filename = prefix + world + suffix;
		BlockNode[][] board = null;
		try { //deserializes .txt into board
			Stream<String> lines = Files.lines(Paths.get(filename));
			int xMax = (int) lines.count(); //find number of lines in the file
			lines.close();
			lines = Files.lines(Paths.get(filename)); //refresh stream
			int yMax = (lines.max( (String s1, String s2) -> { //find maximum length of any specific line
			if(s1.length() > s2.length()){
				return 1;
			}
			else if(s1.length() < s2.length()){
				return -1;
			}
			else{
				return 0;
			}
			})).get().length();
			board = new BlockNode[xMax][yMax];
			lines.close();
			lines = Files.lines(Paths.get(filename)); //refresh stream
			Iterator<String> fileIterator = lines.iterator();
			int i = 0;
			while(fileIterator.hasNext()){ //iterate through stream and construct blockNodes from chars
				String currStr = fileIterator.next();
				for(int j = 0;j < currStr.length();j++){
					char currChar = currStr.charAt(j);
					BlockNode currBlock = new BlockNode(i,j,currChar);
					board[i][j] = currBlock;
				}
				i++;
			}
			lines.close();
		} catch (IOException e) {
			System.out.println("File not found.");
		} 
		return board;
	}
	
	public boolean equals(Object o){
		if(!(o instanceof BlockNode)){
			return false;
		}
		else{
			BlockNode other = (BlockNode) o;
			return this.getX() == other.getX() && this.y == other.y;
		}
	}
	
	public String toString(){
		String output = "";
		if(type == OPEN){
			output += "O";
		}
		else if(type == WALL){
			output += "X";
		}
		else{
			output += "G";
		}
		return output;
	}
	
	public static void printBoard(BlockNode[][] board){
		for(int i = 0;i < board.length;i++){
			for(int j = 0;j < board[i].length;j++){
				System.out.print(board[i][j]);
			}
			System.out.println();
		}
	}
	
	public static String stringBoard(BlockNode[][] board){
		String ret = "";
		for(int i = 0;i < board.length;i++){
			for(int j = 0;j < board[i].length;j++){
				ret = ret + board[i][j];
			}
			ret = ret + "\n";
		}
		return ret;
	}

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getType(){
		return type;
	}
	

}
