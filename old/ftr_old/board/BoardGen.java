package ftr_old.board;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class BoardGen {
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String prefix = "resources/generated_world";
		String suffix = ".txt";
		for(int i = 1; i <= 50 ; i++){
			String filename = prefix + i + suffix;
			try {
				PrintWriter out = new PrintWriter(filename);
				BlockNode[][] board = BlockNode.generateBoard(5, 5);
				//blockNode.printBoard(board);
				//System.out.println("");
				String str = BlockNode.stringBoard(board);
				out.print(str);
				out.close();
			} catch (FileNotFoundException e) {
				System.out.println("Invalid filename.");
			}
			
		}
	}

}
