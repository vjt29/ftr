package ftr_old;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import ftr_old.board.BlockNode;
import ftr_old.graphics.Screen;

public class GraphicsTester {

	public static void main(String[] args) {

		// board setup
		BlockNode[][] board = BlockNode.deserializeBoard(1);

		// screen parameters
		int screenWidth = 1000;
		int screenHeight = 1000;
		// the larger dimension of width vs length for both screen and board
		int cellSizePixels = (screenWidth < screenHeight ? screenWidth : screenHeight)
				/ (board.length < board[0].length ? board.length : board[0].length);

		// readying the window
		JFrame jf = new JFrame("A* Implementation");
		jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		// initialize
		Screen screen = new Screen(board, cellSizePixels);
		
		// creating a layout
		JPanel centralPanel = new JPanel(new BorderLayout());
		centralPanel.add(screen, BorderLayout.CENTER);
		centralPanel.setBackground(Color.WHITE);

		// adding layout to frame
		jf.add(centralPanel, BorderLayout.CENTER);
		jf.setSize(screenWidth, screenHeight);
		
		// make visible
		jf.setVisible(true);
	}
}
