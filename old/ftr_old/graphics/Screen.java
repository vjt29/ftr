package ftr_old.graphics;

/* Library imports */
import javax.swing.JPanel;

import ftr_old.board.BlockNode;

import java.awt.Color;
import java.awt.Graphics;

public class Screen extends JPanel {
	
	// serializeable stuff
	private static final long serialVersionUID = 1L;
	
	// class variables
	private BlockNode[][] board;
	private final int scaleFactor;
	private final int width;
	private final int height;

	public Screen(BlockNode[][] board, int scaleFactor) {
		this.board = board;
		this.scaleFactor = scaleFactor;
		this.width = board.length * scaleFactor;
		this.height = board[0].length * scaleFactor;
	}

	public void paint(Graphics g) {
		// iterate over board, color cells according to value
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j].getType() == BlockNode.GOAL) {
					g.setColor(Color.GREEN);
				} else if (board[i][j].getType() == BlockNode.WALL) {
					g.setColor(Color.BLACK);
				}else {
					g.setColor(Color.WHITE);
				}

				g.fillRect(i * scaleFactor, j * scaleFactor, scaleFactor, scaleFactor);
			}
		}

}
}
