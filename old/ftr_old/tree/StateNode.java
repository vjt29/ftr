package ftr_old.tree;

import ftr_old.board.BlockNode;
import ftr_old.datastructures.Heuristic;

public class StateNode {

	private BlockNode state; //the position of this node
	private StateNode parent; //the parent node
	int action; //action taken to get from parent to child, 0 = no action, initial state, 1 = up, 2 = right, 3 = down, 4 = left
	private int cost; //the cost of going from the root to this state, here cost = depth
	private double heuScore;
	public int searchVal;
	
	/**
	 * Constructor for the root of a tree
	 * @param state the state relating to this tree's node
	 */
	public StateNode(BlockNode state){
		this.state = state;
		this.cost = 1;
		
	}
	
	public StateNode(BlockNode state, int cost){
		this.state = state;
		this.cost = cost;
		
	}
	
	/**
	 * Constructor for any non-root state nodes
	 * @param state the state relating to this tree's node
	 * @param parent the parent of this node
	 * @param action the action taken to get to this node
	 */
	public StateNode(BlockNode state, StateNode parent, int action){
		this.state = state;
		this.parent = parent;
		this.cost = parent.cost + 1; //all actions cost 1
		this.action = action;
	}
	
	/**
	 * @param heuristic the heuristic function to be used to calculate the heuristic score
	 * @param goal the goal node
	 */
	public void calcHeuristic(Heuristic<StateNode,BlockNode> heuristic, BlockNode goal,BlockNode[][] board){
		this.heuScore = heuristic.eval(this, goal,board); 
	}

	public StateNode getParent() {
		return parent;
	}

	public void setParent(StateNode parent) {
		this.parent = parent;
	}

	public BlockNode getState() {
		return state;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public void setState(BlockNode state) {
		this.state = state;
	}
	
	public String toString(){
		double total = this.cost + this.heuScore;
		return state.toString() + total;
	}
	
	public boolean equals(Object o){
		if(!(o instanceof StateNode)){
			return false;
		}
		else{
			StateNode other = (StateNode) o;
			return this.state.equals(other.state);
		}
	}
	
	public int compareTo(Object o){
		if(o instanceof StateNode == false){
			return 1;
		}
		else{
			StateNode other = (StateNode) o;
		if(this.cost + this.heuScore < other.cost + other.heuScore){
			return -1;
		}
		else if(this.cost + this.heuScore > other.cost + other.heuScore){
			return 1;
		}
		else{
			return 0;
		}
		}
	}
		
	


	public double getHeuScore() {
		return heuScore;
	}

	public void setHeuScore(double heuScore) {
		this.heuScore = heuScore;
	}
}
