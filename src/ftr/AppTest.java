package ftr;

import java.io.Serializable;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AppTest extends Application implements Serializable {
	
	private static final long serialVersionUID = 1L;
	Stage mainStage;
	Controller control;
	Scene scene;
	@Override
	public void start(Stage s) {
		mainStage = s;
		try {
			FXMLLoader loader = new FXMLLoader();
			
			//set up login scene
			loader.setLocation(
			         getClass().getResource("View.fxml"));
			AnchorPane root = (AnchorPane)loader.load();
			scene = new Scene(root);
			control = loader.getController();
			control.start();
			
			mainStage.setScene(scene);
			mainStage.setTitle("Login");
			mainStage.setResizable(false);
			mainStage.show();
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		AppTest at = new AppTest();
		launch(args);
	}

}
