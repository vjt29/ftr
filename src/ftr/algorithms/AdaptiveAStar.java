package ftr.algorithms;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;

import ftr.board.BlockNode;
import ftr.board.Board;
import ftr.graphics.MazeDisplay;
import ftr.tree.StateNode;

public class AdaptiveAStar extends RepeatedForwardAStar{

	public double[][] heuristicScore;
	
	public AdaptiveAStar(Board board) {
		super(board);
		heuristicScore = new double[board.board.length][board.board[0].length];

	}
	
	public AdaptiveAStar(Board board, Comparator<StateNode> comparator, String message) {
		super(board,comparator,message);
		heuristicScore = new double[board.board.length][board.board[0].length];
	}
	
	
	protected void computePath(StateNode start, StateNode goal) {
		// System.out.println("Compute Invoked");
		boolean found = false;
		// storing all StateNodes generated in a given pass
		HashMap<BlockNode, StateNode> encounteredStates = new HashMap<>();
		openSet.add(goal);
		// run while the estimated path cost is greated than the minimum
		// computed cost
		while (openSet.peek() != null && goal.g > openSet.peek().f) {
			
			// remove a state s with the smallest f-value from OPEN
			StateNode s = openSet.poll();
			if(closedSet.contains(s)){
				continue;
			}
	
			// adding set to list of encountered states
			encounteredStates.put(s.bn, s);
	
			// add S to the closed list
			// actually adds the associated blocknode
			closedSet.add(s.bn);
			expanded++;
	
			// considering all potential actions available from the current
			// state (s)
			for (BlockNode neighbor : s.getOpenNeighbors()) {
				//System.out.println(neighbor.toString());
				// getting the associated StateNode
				StateNode successor;
				if (neighbor.type == BlockNode.GOAL){
					successor = goal;
					found = true;
				} else if (encounteredStates.containsKey(neighbor)) {
					// if the state has been encountered already, use it
					successor = encounteredStates.get(neighbor);
					//System.out.println("encounter");
				} else {
					// if not, initialize a new one
					successor = new StateNode(
							neighbor, 
							naiveBoard, 
							null, 
							0, 
							Double.MAX_VALUE, 
							naiveBoard.manhattanDistance(neighbor),
							comparator);
					if(heuristicScore[neighbor.x][neighbor.y] > 0){
						successor.h = heuristicScore[neighbor.x][neighbor.y];
					}
					encounteredStates.put(successor.bn, successor);
				}
				
				// check of counter value
				if (successor.search < counter) {
					successor.g = Double.MAX_VALUE;
					successor.search = counter;
				}
				
				// check if the transition cost makes successor viable
				if (successor.g > s.g + naiveBoard.getTransitionCost(s.bn, successor.bn)) {
					// update path cost for successor
					//System.out.print("updating cost from " + successor.g);
					successor.g = s.g + naiveBoard.getTransitionCost(s.bn, successor.bn);
					//System.out.println(" to " + successor.g);
					// update the path tree
					successor.parent = s;
					// check if successor is in the open list
					// remove it if that is the case
					StateNode toBeRemoved = null;
					for (StateNode sn : openSet) {
						if (successor.bn.equals(sn.bn)) {
							toBeRemoved = sn;
						}
					}
					openSet.remove(toBeRemoved);
					// insert successor into the open list
					openSet.add(successor);
				}
			}
		}
		if(!(found)){
			System.out.println("No path.");
		}
		encounteredStates.forEach((bn, sn) -> {
			if(closedSet.contains(bn)){
			heuristicScore[bn.x][bn.y] = goal.g - sn.g;
			}
		});
	}

}
