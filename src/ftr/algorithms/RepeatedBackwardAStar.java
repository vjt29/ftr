package ftr.algorithms;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;

import ftr.board.BlockNode;
import ftr.board.Board;
import ftr.tree.StateNode;

public class RepeatedBackwardAStar extends RepeatedForwardAStar{

	

	public RepeatedBackwardAStar(Board board, Comparator<StateNode> comparator, String message) {
		super(board.deepCopy().swapStartAndGoal(), comparator, message);
	}
	
	public LinkedList<StateNode> eval() {
		// displaying the maze
		if(graphics){
			mazeDisplay.show();
			mazeDisplay.setFinalPath(finalPath);
		}
		
		// initializations
		counter = 0;
		naiveBoard = new Board(actualBoard.board.length, actualBoard.board.length);

		// setting start and goal states for actual board
		if (actualBoard.getGoal() == null) {
			actualBoard.setGoal(0, 0);
		}
		if (actualBoard.getStart() == null) {
			actualBoard.setStart(actualBoard.board.length, actualBoard.board.length);
		}

		// setting start and goal states for actual and naive board
		naiveBoard.setGoal(actualBoard.goal.x, actualBoard.goal.y);
		naiveBoard.setStart(actualBoard.start.x, actualBoard.start.y);

		// main loop
		while (!actualBoard.start.equals(actualBoard.goal) && pathExists) {
			// program counter
			counter += 1;

			// start state
			StateNode startState = new StateNode(
					naiveBoard.start, // BlockNode
					naiveBoard,       // parent Board
					null,             // parent StateNode
					counter,          // program counter
					0,                // initial g-value
					naiveBoard.manhattanDistance(naiveBoard.start), // heuristic
					comparator);      // comparator

			// start state
			StateNode goalState = new StateNode(
					naiveBoard.goal,  // BlockNode
					naiveBoard,       // parent Board
					null,             // parent StateNode (will need to be updated in computePath())
					counter,          // program counter
					Double.MAX_VALUE, // initial g-value
					naiveBoard.manhattanDistance(naiveBoard.goal), // heuristic
					comparator);      // comparator

			// clearing out the open and closed sets
			openSet = new PriorityQueue<>();
			closedSet = new HashSet<>();

			// adding start state to open list
			openSet.add(startState);
			
			// populating naive board with immediately adjascent cells
			StateNode realStart = new StateNode(actualBoard.start, actualBoard, null, 0, 0, 0);
			BlockNode[] neighbors = realStart.getNeighbors();
			for(BlockNode neighbor : neighbors){
				naiveBoard.board[neighbor.x][neighbor.y].type = neighbor.type;
				//System.out.println(neighbor);
			}

			// single round of RFA*
			computePath(startState, goalState);
			//System.out.println("Path Computed");

			// checking for a possible path
			// if none, return null
			if (openSet.isEmpty()) {
				System.out.println("No Possible Path");
				return null;
			}

			// progressing along the path until an obstruction is found
			// updating values as it progresses
			progress(goalState);
			//System.out.println("Progress");
			//return null;
		}

		return null;
	}
	
	private void progress(StateNode goal) {
		naivePath = buildPath(goal);

		// updating display
		if (graphics) {
			mazeDisplay.setPath(naivePath);
			mazeDisplay.repaint();
		}

		// moving through the linked list
		for (BlockNode ptr : naivePath) {
			// avoiding redundant motion
			if (ptr.equals(actualBoard.goal)) {
				continue;
			}
			
			// adjacency
			if(ptr.type == BlockNode.START){
				StateNode adjacencyTest = new StateNode(actualBoard.start, actualBoard, null, 0, 0, 0);
				boolean indicator = false;
				for(BlockNode n : adjacencyTest.getNeighbors()){
					if(n.type == BlockNode.START){
						//System.out.println("True");
						indicator = true;
					}
				}
				if(indicator == false){
					//System.out.println("not adjascent - B");
					pathExists = false;
					return;
				}
			}

			// updating naiveBoard
			updateNaiveBoard(ptr);

			// moving the goal state if there are no obstacles
			if (ptr.type != BlockNode.WALL) {
				// updating start position on board
				actualBoard.setGoal(ptr.x, ptr.y);
				naiveBoard.setGoal(ptr.x, ptr.y);
				finalPath.add(ptr);

				// checking for path completion
				if (actualBoard.goal.equals(actualBoard.start)) {
					//System.out.println("Path Complete!");
				}

			} else {
				// the path cannot progress any further, return and recompute
				return;
			}

			// updating display
			if (true) {
				
				try {
					Thread.sleep(clockSpeed);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//mazeDisplay.repaint();
			}

		}
	}
	
	private LinkedList<BlockNode> buildPath(StateNode goal){
		LinkedList<BlockNode> path = new LinkedList<>();
		StateNode ptr = goal;
		while(ptr != null){
			path.add(ptr.bn);
			ptr = ptr.parent;
		}
		return path;
	}
	
}
