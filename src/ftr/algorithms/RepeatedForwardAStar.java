package ftr.algorithms;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;

import ftr.board.BlockNode;
import ftr.board.Board;
import ftr.graphics.MazeDisplay;
import ftr.tree.StateNode;

public class RepeatedForwardAStar {

	// class variables
	public Board actualBoard;
	public int expanded;
	public Board naiveBoard;
	protected PriorityQueue<StateNode> openSet;
	// BlockNode because StateNodes are instantiated on an as-needed basis
	protected HashSet<BlockNode> closedSet; 
	protected int counter;
	// StateNode comparator
	protected Comparator<StateNode> comparator = null;
	// graphical display
	protected boolean graphics = false;
	protected int clockSpeed = 25;
	public MazeDisplay mazeDisplay;
	public String message;
	// infinite loop prevention
	protected boolean pathExists = true;
	// final path
	public LinkedList<BlockNode> finalPath = new LinkedList<>();
	public LinkedList<BlockNode> naivePath;

	public RepeatedForwardAStar(Board board) {
		this.actualBoard = board;
		openSet = new PriorityQueue<>();
		closedSet = new HashSet<>();
	}
	
	public RepeatedForwardAStar(Board board, Comparator<StateNode> comparator, String message) {
		this.actualBoard = board;
		this.comparator = comparator;
		this.message = message;
		openSet = new PriorityQueue<StateNode>(10);
		closedSet = new HashSet<>();
	}

	public void enableGraphics() {
		graphics = true;
		mazeDisplay = new MazeDisplay(actualBoard, 1000, 1000, message);
	}
	
	public void setClockSpeed(int clockSpeed){
		if(clockSpeed >= 1){
			this.clockSpeed = clockSpeed;
		}
	}

	/**
	 * runs the RFA* algorithm
	 * 
	 * @param graphics
	 *            true if a graphical animation is desired
	 * @return
	 */
	public LinkedList<StateNode> eval() {
		// displaying the maze
		if(graphics){
			mazeDisplay.show();
			mazeDisplay.setFinalPath(finalPath);
		}
		
		// initializations
		counter = 0;
		naiveBoard = new Board(actualBoard.board.length, actualBoard.board.length);

		// setting start and goal states for actual board
		if (actualBoard.getGoal() == null) {
			actualBoard.setGoal(0, 0);
		}
		if (actualBoard.getStart() == null) {
			actualBoard.setStart(actualBoard.board.length, actualBoard.board.length);
		}

		// setting start and goal states for actual and naive board
		naiveBoard.setGoal(actualBoard.goal.x, actualBoard.goal.y);
		naiveBoard.setStart(actualBoard.start.x, actualBoard.start.y);

		// main loop
		while (!actualBoard.start.equals(actualBoard.goal) && pathExists) {
			// program counter
			counter += 1;

			// start state
			StateNode startState = new StateNode(
					naiveBoard.start, // BlockNode
					naiveBoard,       // parent Board
					null,             // parent StateNode
					counter,          // program counter
					0,                // initial g-value
					naiveBoard.manhattanDistance(naiveBoard.start), // heuristic
					comparator);      // comparator

			// start state
			StateNode goalState = new StateNode(
					naiveBoard.goal,  // BlockNode
					naiveBoard,       // parent Board
					null,             // parent StateNode (will need to be updated in computePath())
					counter,          // program counter
					Double.MAX_VALUE, // initial g-value
					naiveBoard.manhattanDistance(naiveBoard.goal), // heuristic
					comparator);      // comparator

			// clearing out the open and closed sets
			openSet = new PriorityQueue<>();
			closedSet = new HashSet<>();

			// adding start state to open list
			openSet.add(startState);
			
			// populating naive board with immediately adjascent cells
			StateNode realStart = new StateNode(actualBoard.start, actualBoard, null, 0, 0, 0);
			BlockNode[] neighbors = realStart.getNeighbors();
			for(BlockNode neighbor : neighbors){
				naiveBoard.board[neighbor.x][neighbor.y].type = neighbor.type;
				//System.out.println(neighbor);
			}

			// single round of RFA*
			computePath(startState, goalState);
			//System.out.println("Path Computed");

			// checking for a possible path
			// if none, return null
			if (openSet.isEmpty()) {
				System.out.println("No Possible Path");
				return null;
			}

			// progressing along the path until an obstruction is found
			// updating values as it progresses
			progress(goalState);
			//System.out.println("Progress");
			//return null;
		}

		return null;
	}

	protected void computePath(StateNode start, StateNode goal) {
		// System.out.println("Compute Invoked");
		boolean found = false;
		// storing all StateNodes generated in a given pass
		HashMap<BlockNode, StateNode> encounteredStates = new HashMap<>();
		openSet.add(goal);
		// run while the estimated path cost is greated than the minimum
		// computed cost
		while (openSet.peek() != null && goal.g > openSet.peek().f) {
			
			// remove a state s with the smallest f-value from OPEN
			StateNode s = openSet.poll();
			if(closedSet.contains(s)){
				continue;
			}

			// adding set to list of encountered states
			encounteredStates.put(s.bn, s);

			// add S to the closed list
			// actually adds the associated blocknode
			closedSet.add(s.bn);
			expanded++;

			// considering all potential actions available from the current
			// state (s)
			for (BlockNode neighbor : s.getOpenNeighbors()) {
				//System.out.println(neighbor.toString());
				// getting the associated StateNode
				StateNode successor;
				if (neighbor.type == BlockNode.GOAL){
					successor = goal;
					found = true;
				} else if (encounteredStates.containsKey(neighbor)) {
					// if the state has been encountered already, use it
					successor = encounteredStates.get(neighbor);
					//System.out.println("encounter");
				} else {
					// if not, initialize a new one
					successor = new StateNode(
							neighbor, 
							naiveBoard, 
							null, 
							0, 
							Double.MAX_VALUE, 
							naiveBoard.manhattanDistance(neighbor),
							comparator);
					encounteredStates.put(successor.bn, successor);
				}
				
				// check of counter value
				if (successor.search < counter) {
					successor.g = Double.MAX_VALUE;
					successor.search = counter;
				}
				
				// check if the transition cost makes successor viable
				if (successor.g > s.g + naiveBoard.getTransitionCost(s.bn, successor.bn)) {
					// update path cost for successor
					//System.out.print("updating cost from " + successor.g);
					successor.g = s.g + naiveBoard.getTransitionCost(s.bn, successor.bn);
					//System.out.println(" to " + successor.g);
					// update the path tree
					successor.parent = s;
					// check if successor is in the open list
					// remove it if that is the case
					StateNode toBeRemoved = null;
					for (StateNode sn : openSet) {
						if (successor.bn.equals(sn.bn)) {
							toBeRemoved = sn;
						}
					}
					openSet.remove(toBeRemoved);
					// insert successor into the open list
					openSet.add(successor);
				}
			}
		}
		if(!(found)){
			//System.out.println("No path.");
			pathExists = false;
		}
	}

	private void progress(StateNode goal) {
		naivePath = buildPath(goal);

		// updating display
		if (graphics) {
			mazeDisplay.setPath(naivePath);
			mazeDisplay.repaint();
		}

		// moving through the linked list
		for (BlockNode ptr : naivePath) {
			// avoiding redundant motion
			if (ptr.equals(actualBoard.start)) {
				continue;
			}
			
			// adjacency
			if(ptr.type == BlockNode.GOAL){
				StateNode adjacencyTest = new StateNode(actualBoard.start, actualBoard, null, 0, 0, 0);
				boolean indicator = false;
				for(BlockNode n : adjacencyTest.getNeighbors()){
					if(n.type == BlockNode.GOAL){
						//System.out.println("True");
						indicator = true;
					}
				}
				if(indicator == false){
					//System.out.println("not adjascent");
					pathExists = false;
					return;
				}
			}

			// updating naiveBoard
			updateNaiveBoard(ptr);

			// moving the start state if there are no obstacles
			if (ptr.type != BlockNode.WALL) {
				// updating start position on board
				actualBoard.setStart(ptr.x, ptr.y);
				naiveBoard.setStart(ptr.x, ptr.y);
				finalPath.add(ptr);

				// checking for path completion
				if (actualBoard.start.equals(actualBoard.goal)) {
					//System.out.println("Path Complete!");
				}

			} else {
				// the path cannot progress any further, return and recompute
				return;
			}

			// updating display
			if (true) {
				
				try {
					Thread.sleep(clockSpeed);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//mazeDisplay.repaint();
			}

		}

	}

	protected void updateNaiveBoard(BlockNode bn) {
		StateNode sn = new StateNode(bn, actualBoard, null, 0, 0, 0);
		BlockNode[] neighbors = sn.getNeighbors();
		for (BlockNode neighbor : neighbors) {
			if (neighbor.type == BlockNode.WALL) {
				naiveBoard.board[neighbor.x][neighbor.y].changeType(BlockNode.WALL);
			}
		}
	}
	
	private LinkedList<BlockNode> buildPath(StateNode goal){
		LinkedList<BlockNode> path = new LinkedList<>();
		StateNode ptr = goal;
		while(ptr != null){
			path.addFirst(ptr.bn);
			ptr = ptr.parent;
		}
		return path;
	}

}
