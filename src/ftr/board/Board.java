package ftr.board;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Stack;
import java.util.stream.Stream;

public class Board {

	public BlockNode[][] board;
	public BlockNode goal;
	public BlockNode start;

	/**
	 * constructor
	 * 
	 * @param world
	 *            a previously generated world that will be loaded into memory
	 */
	public Board(int world) {
		this.board = deserializeBoard(world);
	}

	/**
	 * constructor for an empty board
	 * 
	 * @param x
	 *            the width of board
	 * @param y
	 *            the height of the board
	 */
	public Board(int x, int y) {
		this.board = new BlockNode[x][y];
		// initializing all cells to blank
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				this.board[i][j] = new BlockNode(i, j, 'O');
			}
		}
	}

	/**
	 * finds the goal state of a board (if it is not already known)
	 * 
	 * @return the BlockNode corresponding to the goal state
	 */
	public BlockNode getGoal() {
		// checking if the goal is already known
		if (goal != null) {
			return goal;
		}

		// searching the board
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (board[i][j].type == BlockNode.GOAL) {
					goal = board[i][j];
					return goal;
				}
			}
		}

		// return NULL if no goal found
		return null;
	}

	/**
	 * sets (x,y) to the goal state. Removes other goal states from the board
	 * board
	 * 
	 * @param x
	 * @param y
	 */
	public void setGoal(int x, int y) {
		// error handling
		try {
			if (board[x][y] == null) {
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// clearing the board
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (board[i][j].type == BlockNode.GOAL) {
					board[i][j].changeType(BlockNode.OPEN);
				}
			}
		}

		// setting the new goal
		goal = board[x][y];
		goal.changeType(BlockNode.GOAL);
	}

	/**
	 * finds the start state of a board (if it is not already known)
	 * 
	 * @return the BlockNode corresponding to the start state
	 */
	public BlockNode getStart() {
		// checking if the goal is already known
		if (start != null) {
			return start;
		}

		// searching the board
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (board[i][j].type == BlockNode.START) {
					start = board[i][j];
					return start;
				}
			}
		}

		// return NULL if no start found
		return null;
	}

	/**
	 * sets (x,y) to the start state. Removes other start states from the board
	 * board
	 * 
	 * @param x
	 * @param y
	 */
	public void setStart(int x, int y) {
		// error handling
		try {
			if (board[x][y] == null) {
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// clearing the board
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (board[i][j].type == BlockNode.START) {
					board[i][j].changeType(BlockNode.OPEN);
				}
			}
		}

		// setting the new goal
		start = board[x][y];
		start.changeType(BlockNode.START);
	}

	/**
	 * @param bn
	 *            a blocknode on the board
	 * @return the manhattan distance heuristic
	 */
	public double manhattanDistance(BlockNode bn) {
		return Math.abs(bn.x - goal.x) + Math.abs(bn.y - goal.y);
	}

	/**
	 * @return a string representation of the board
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				sb.append(board[i][j].toString());
			}
			sb.append("\n");
		}

		return sb.toString();
	}
	
	/**
	 * @return an identical copy of the board
	 */
	public Board deepCopy() {
		Board copy = new Board(board.length, board[0].length);
		for(int i = 0; i < copy.board.length; i++){
			for(int j = 0; j < copy.board.length; j++){
				copy.board[i][j].type = this.board[i][j].type;
				if(copy.board[i][j].type == BlockNode.START){
					copy.setStart(i, j);
				} else if(copy.board[i][j].type == BlockNode.GOAL){
					copy.setGoal(i, j);
				}
			}
		}
		return copy;
	}
	
	public Board swapStartAndGoal(){
		if(goal == null || start == null){
			return null;
		}
		
		int goalX = goal.x;
		int goalY = goal.y;
		int startX = start.x;
		int startY = start.y;
		
		setGoal(startX, startY);
		setStart(goalX, goalY);
		
		return this;
	}

	/**
	 * The transition cost between two nodes on the board
	 * 
	 * @param bn1
	 * @param bn2
	 * @return the transition cost between the two BlockNodes as a double
	 */
	public double getTransitionCost(BlockNode bn1, BlockNode bn2) {
		return 1.0;
	}

	/*
	 * *************************************************************** 
	 * STATIC METHODS 
	 * ***************************************************************
	 */

	/**
	 * generates a board with dimensions x and y
	 * 
	 * @param x
	 *            width of board
	 * @param y
	 *            height of the board
	 * @return an array of type <BlockNode> representing a maze
	 */
	public static BlockNode[][] generateBoard(int x, int y) {
		BlockNode[][] board = new BlockNode[x][y]; // the actual board
		boolean[][] visited = new boolean[x][y]; // a visited matrix
		int visitednum = 0; // the number of visited blocks
		// the stack used for dfs
		Stack<BlockNode> stack = new Stack<BlockNode>();
		for (int i = 0; i < x; i++) { // generate a new board
			for (int j = 0; j < y; j++) {
				board[i][j] = new BlockNode(i, j);
			}
		}

		Random rng = new Random(); // randomly generate a starting position
		int startx = rng.nextInt(x);
		int starty = rng.nextInt(y);
		stack.add(board[startx][starty]);
		while (visitednum < x * y) {
			while (!(stack.isEmpty())) {
				BlockNode curr = stack.pop();
				int currX = curr.x;
				int currY = curr.y;
				if (!(visited[currX][currY])) {
					visited[currX][currY] = true;
					visitednum++;
				}
				ArrayList<BlockNode> neighbors = new ArrayList<BlockNode>();
				// populate neighbor list with non-visited neighbors
				if (currX > 0) { // add left if left in bounds
					if (visited[currX - 1][currY] == false) {
						neighbors.add(board[currX - 1][currY]);
					}
				}
				if (currX < x - 1) { // add right if right in bounds
					if (visited[currX + 1][currY] == false) {
						neighbors.add(board[currX + 1][currY]);
					}
				}
				if (currY > 0) {// add below if below in bounds
					if (visited[currX][currY - 1] == false) {
						neighbors.add(board[currX][currY - 1]);
					}
				}
				if (currY < y - 1) {// add up if up in bounds
					if (visited[currX][currY + 1] == false) {
						neighbors.add(board[currX][currY + 1]);
					}
				}

				// pick a random neighbor and either block it, or add at it to
				// the stack
				if (neighbors.size() > 0) {
					int neighborRNG = rng.nextInt(neighbors.size());
					BlockNode neighbor = neighbors.get(neighborRNG);
					int neighborX = neighbor.x;
					int neighborY = neighbor.y;
					visited[neighborX][neighborY] = true;
					visitednum++;
					Double blockChance = rng.nextDouble();
					if (blockChance <= .3) {
						neighbor.changeType(BlockNode.WALL);
						stack.add(curr);
					} else {
						// add the parent again before the neighbor to backtrack
						// if any neighbors end up unvisited
						stack.add(curr);
						stack.add(neighbor); // add the neighbor
					}
				}

			}

			// if the stack is empty and not all blocks have been traversed,
			// find the first node not touched and add it to the stack, then
			// break, else all nodes touched
			for (int i = 0; i < x; i++) {
				for (int j = 0; j < y; j++) {
					if (visited[i][j] == false) {
						stack.add(board[i][j]);
						break;
					}
				}
			}

		}
		board[0][0].type = BlockNode.START;
		board[1][0].type = BlockNode.OPEN;
		board[0][1].type = BlockNode.OPEN;
		board[board.length-1][board[board.length-1].length-1].type= BlockNode.GOAL;
		board[board.length-2][board[board.length-1].length-1].type = BlockNode.OPEN;
		board[board.length-1][board[board.length-2].length-2].type = BlockNode.OPEN;
		return board;
	}

	/**
	 * Opens a .txt file, parses it, and converts it to objects that can be used
	 * as a game board.
	 * 
	 * @param world
	 *            the world to be deserialized, only numbers which correspond to
	 *            a previously generated world in the resources folder are valid
	 * @return a 2d array made up of blockNodes, generated from the .txt file
	 *         opened
	 */
	@SuppressWarnings("resource")
	public static BlockNode[][] deserializeBoard(int world) {
		String prefix = "resources/generated_world";
		String suffix = ".txt";
		String filename = prefix + world + suffix;
		BlockNode[][] board = null;
		try { // deserializes .txt into board
			Stream<String> lines = Files.lines(Paths.get(filename));
			int xMax = (int) lines.count(); // find number of lines in the file
			lines.close();
			lines = Files.lines(Paths.get(filename)); // refresh stream
			// find maximum length of any specific line
			int yMax = (lines.max((String s1, String s2) -> {
				if (s1.length() > s2.length()) {
					return 1;
				} else if (s1.length() < s2.length()) {
					return -1;
				} else {
					return 0;
				}
			})).get().length();
			board = new BlockNode[xMax][yMax];
			lines.close();
			lines = Files.lines(Paths.get(filename)); // refresh stream
			Iterator<String> fileIterator = lines.iterator();
			int i = 0;
			// iterate through stream and construct blockNodes from chars
			while (fileIterator.hasNext()) {
				String currStr = fileIterator.next();
				for (int j = 0; j < currStr.length(); j++) {
					char currChar = currStr.charAt(j);
					BlockNode currBlock = new BlockNode(i, j, currChar);
					board[i][j] = currBlock;
				}
				i++;
			}
			lines.close();
		} catch (IOException e) {
			System.out.println("File not found.");
		}
		return board;
	}
}
