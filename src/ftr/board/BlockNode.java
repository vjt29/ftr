package ftr.board;

public class BlockNode {

	public static final int OPEN = 0;
	public static final int WALL = 1;
	public static final int GOAL = 2;
	public static final int START = 3;

	public int type; // type of board piece, 0 = normal, 1 = wall, 2 = goal
	public int x;
	public int y;

	public BlockNode(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public BlockNode(int x, int y, char type) {
		this.x = x;
		this.y = y;
		if (type == 'G') {
			this.type = GOAL;
		} else if (type == 'X') {
			this.type = WALL;
		} else if (type == 'S') {
			this.type = START;
		} else if (type == 'O') {
			this.type = OPEN;
		} else {
			this.type = WALL;
		}
	}

	/**
	 * Changes the type of block to a different type
	 * 
	 * @param type
	 *            the new type to change to
	 */
	public void changeType(int type) {
		this.type = type;
	}

	public boolean equals(Object o) {
		if (!(o instanceof BlockNode)) {
			return false;
		} else {
			BlockNode other = (BlockNode) o;
			return this.x == other.x && this.y == other.y;
		}
	}

	public String toString() {
		String output = "";
		if (type == OPEN) {
			output += "O";
		} else if (type == WALL) {
			output += "X";
		} else if (type == START) {
			output += "S";
		} else if (type == GOAL) {
			output += "G";
		} else {
			output += "?";
		}
		output += ":(" + x + "," + y+")";
		return output;
	}

}
