package ftr.board;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class BoardGen {
	

	public static String stringBoard(BlockNode[][] board){
		String ret = "";
		for(int i = 0;i < board.length;i++){
			for(int j = 0;j < board[i].length;j++){
					
					if(board[i][j].type == BlockNode.OPEN){
						ret += "O";
					}
					else if(board[i][j].type == BlockNode.WALL){
						ret += "X";
					}
					else if(board[i][j].type == BlockNode.GOAL){
						ret += "G";
					}
					else{
						ret += "S";
					}
				
			}
			ret = ret + "\n";
		}
		return ret;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String prefix = "resources/generated_world";
		String suffix = ".txt";
		for(int i = 1; i <= 50 ; i++){
			String filename = prefix + i + suffix;
			try {
				PrintWriter out = new PrintWriter(filename);
				BlockNode[][] board = Board.generateBoard(101, 101);
				//blockNode.printBoard(board);
				//System.out.println("");
				String str = stringBoard(board);
				out.print(str);
				out.close();
			} catch (FileNotFoundException e) {
				System.out.println("Invalid filename.");
			}
			
		}
	}

}
