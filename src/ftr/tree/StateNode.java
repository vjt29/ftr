package ftr.tree;

import java.util.ArrayList;
import java.util.Comparator;

import ftr.board.BlockNode;
import ftr.board.Board;

public class StateNode implements Comparable<StateNode> {

	// necessary for algorithm
	public BlockNode bn; // associated blocknode
	public Board board; // associated board
	public StateNode parent; // parent of the current state
	public int search; // counter value
	public double g; // cost from the start state to self
	public double h; // heuristic measure from self to goal
	public double f; // f := g + h
	public Comparator<StateNode> comparator = null;

	/**
	 * Represents the metadata needed for a pass of A*
	 * 
	 * @param bn
	 *            a pointer to the BlockNode whose data StateNode is
	 *            representing
	 * @param board
	 *            the board that bn is on
	 * @param parent
	 *            the parent node of the current state
	 * @param search
	 *            the number of timess ComputePath() has been called at the
	 *            creation of StateNode
	 * @param g
	 *            the path cost from the start state to the current state
	 * @param h
	 *            the heuristic measure from the current state to the goal
	 */
	public StateNode(BlockNode bn, Board board, StateNode parent, int search, double g, double h) {
		this.bn = bn;
		this.board = board;
		this.parent = parent;
		this.search = search; // default is zero
		this.g = g;
		this.h = h;
		this.f = g + h;
	}
	
	public StateNode(BlockNode bn, Board board, StateNode parent, int search, double g, double h, Comparator<StateNode> comparator) {
		this.bn = bn;
		this.board = board;
		this.parent = parent;
		this.search = search; // default is zero
		this.g = g;
		this.h = h;
		this.f = g + h;
		this.comparator = comparator;
	}

	/**
	 * recalculates the value of f := g + h
	 * 
	 * @return the recomputed value of f
	 */
	public double recomputeFValue() {
		f = g + h;
		return f;
	}

	@Override
	public int compareTo(StateNode arg0) {
		
		if(comparator == null){
			//System.out.println("CompareTo");
			// ensuring f-values are up-to-date
			this.recomputeFValue();
			arg0.recomputeFValue();
			if (this.f < arg0.f) {
				return -1;
			} else if (this.f > arg0.f) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return comparator.compare(this, arg0);
		}
		
	}

	/**
	 * @return an array containing all adjascent blocknodes
	 */
	public BlockNode[] getNeighbors() {
		ArrayList<BlockNode> neighbors = new ArrayList<>(4);

		if(bn.x - 1 >= 0){
			neighbors.add(board.board[bn.x-1][bn.y]);
		}
		if(bn.y - 1 >= 0){
			neighbors.add(board.board[bn.x][bn.y-1]);
		}
		if(bn.x + 1 < board.board.length){
			neighbors.add(board.board[bn.x+1][bn.y]);
		}
		if(bn.y + 1 < board.board[bn.x].length){
			neighbors.add(board.board[bn.x][bn.y+1]);
		}

		return neighbors.toArray(new BlockNode[neighbors.size()]);
	}
	
	/**
	 * @return an array containing non-wall BlockNodes adjascent to the current StateNode
	 */
	public BlockNode[] getOpenNeighbors(){
		ArrayList<BlockNode> newNeighbors = new ArrayList<>(4);
		
		for(BlockNode neighbor : getNeighbors()){
			if(neighbor.type != BlockNode.WALL){
				//System.out.println("neighbor: " + neighbor.toString() + " " + neighbor.type);
				newNeighbors.add(neighbor);
			}
		}
		
		return newNeighbors.toArray(new BlockNode[newNeighbors.size()]);
	}
	
	public boolean equals(Object o){
		if(o instanceof StateNode == false){
			return false;
		}
		else{
			StateNode other = (StateNode) o;
			return this.bn.equals(other.bn);
		}
	}

}
