package ftr;

import java.time.Clock;
import java.time.Instant;

import ftr.algorithms.AdaptiveAStar;
import ftr.algorithms.RepeatedForwardAStar;
import ftr.board.Board;
import ftr.comparator.FValue;
import ftr.comparator.FValueGFirst;
import ftr.comparator.FValueGRandom;

public class Timer {

	public static void main(String[] args){
		String analysis = "";
		int aasW = 0;
		int rfaW = 0;
		for(int i = 53;i <= 53;i++){
		Board test = new Board(i);
		AdaptiveAStar aas = new AdaptiveAStar(test,new FValueGFirst(),"RFA* - (Blue = Start, Green = Goal, Red = Path, Orange = Wall Collision)");
		aas.eval();
		
		test = new Board(i);
		RepeatedForwardAStar rfa = new RepeatedForwardAStar(test,new FValueGFirst(),"RFA* - (Blue = Start, Green = Goal, Red = Path, Orange = Wall Collision)");
		rfa.eval();

		//System.out.println(((double)(endTime - startTime))/1000000);
		analysis += "Board " + i + ", AA*: " +aas.expanded + ", RFA*: " + rfa.expanded;
		if(aas.expanded < rfa.expanded){
			analysis += ". aas wins";
			aasW++;
		}
		else if(aas.expanded > rfa.expanded){
			analysis += ". rfa wins";
			rfaW++;
		}
		else{
			analysis += ". tie";
		}
		if(aas.expanded < rfa.expanded){
			analysis += ", difference: " + Math.abs(aas.expanded - rfa.expanded);
			analysis += ", percent: " + ((double)Math.abs(aas.expanded - rfa.expanded)/(double)aas.expanded)*100;
		}
		analysis += "\n";
		} 
		System.out.println(analysis);
		System.out.println("AAS wins: " + aasW + ". RFA wins: " + rfaW + ". Ties:" + (50-aasW-rfaW));
	}
	
}
