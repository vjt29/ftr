package ftr.comparator;

import java.util.Comparator;

import ftr.tree.StateNode;

public class FValueGFirst implements Comparator<StateNode> {

	@Override
	public int compare(StateNode o1, StateNode o2) {
		// ensuring f-values are up-to-date
		o1.recomputeFValue();
		o2.recomputeFValue();
		if (o1.f < o2.f) {
			return -1;
		} else if (o1.f > o2.f) {
			return 1;
		} else{
			if(o1.g > o2.g){ //tiebreak in favor of o1 if or.g is larger
				return -1;
			}
			else if(o1.g < o2.g){
				return 1;
			}
			else{
				return -1;
			}
		}
	}

}
