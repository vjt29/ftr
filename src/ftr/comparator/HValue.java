package ftr.comparator;

import java.util.Comparator;

import ftr.tree.StateNode;

public class HValue implements Comparator<StateNode> {

	@Override
	public int compare(StateNode o1, StateNode o2) {
		System.out.println("HValue Comparison");
		// ensuring f-values are up-to-date
		o1.recomputeFValue();
		o2.recomputeFValue();
		if (o1.h < o2.h) {
			return -1;
		} else if (o1.h > o2.h) {
			return 1;
		} else {
			return 0;
		}
	}

}
