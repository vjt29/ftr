package ftr.comparator;

import java.util.Comparator;

import ftr.tree.StateNode;

public class FValueGSmallerRandom implements Comparator<StateNode> {

	@Override
	public int compare(StateNode o1, StateNode o2) {
		// ensuring f-values are up-to-date
		o1.recomputeFValue();
		o2.recomputeFValue();
		if (o1.f < o2.f) {
			return -1;
		} else if (o1.f > o2.f) {
			return 1;
		} else{
			if(o1.g > o2.g){ //tiebreak in favor of o1 if or.g is larger
				return 1;
			}
			else if(o1.g < o2.g){
				return -1;
			}
			else{
				double random = Math.random(); //tiebreak using a random number, 50% to pick o1 as smaller, 50% to pick o2 as smaller 
				if(random <= .5){
					return -1;
				}
				else{
					return 1;
				}
			}
		}
	}

}
