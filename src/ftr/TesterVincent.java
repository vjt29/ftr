package ftr;

import ftr.algorithms.AdaptiveAStar;
import ftr.algorithms.RepeatedForwardAStar;
import ftr.board.Board;

public class TesterVincent {

	public static void main(String[] args) {
		Board test = new Board(1);
		System.out.println(test.goal);
		System.out.println(test.start);
		AdaptiveAStar rfa = new AdaptiveAStar(test);
		rfa.enableGraphics();
		rfa.setClockSpeed(10);
		rfa.eval();

	}

}
