package ftr;

import ftr.algorithms.AdaptiveAStar;
import ftr.algorithms.RepeatedBackwardAStar;
import ftr.algorithms.RepeatedForwardAStar;
import ftr.board.Board;
import ftr.comparator.FValue;
import ftr.comparator.FValueGFirst;
import ftr.comparator.FValueGRandom;

public class TesterKyle {

	public static void main(String[] args) {
		/* Comparing RFA to RBA */
		// variables
		int[][] expanded = new int[50][2];
		RepeatedForwardAStar rfa;
		RepeatedBackwardAStar rba;

		// testing over 50 generated boards
		for (int i = 1; i <= 50; i++) {
			// initialization
			rfa = new RepeatedForwardAStar(new Board(i), new FValueGRandom(), "RFA (" + i + ")");
			rba = new RepeatedBackwardAStar(new Board(i), new FValueGRandom(), "RBA (" + i + ")");
			
			rfa.enableGraphics();
			rfa.setClockSpeed(50);
			
			rba.enableGraphics();
			rba.setClockSpeed(50);
			
			// evaluation
			rfa.eval();
			rba.eval();

			// storing results
			expanded[i-1][0] = rfa.expanded;
			expanded[i-1][1] = rba.expanded;
			
			System.out.println(i + "\t" + rfa.expanded + "\t" + rba.expanded);
		}
		
		// averaging
		double avgRFA = 0;
		double avgRBA = 0;
		for(int i = 0; i < expanded.length; i++){
			avgRFA += expanded[i][0];
			avgRBA += expanded[i][1];
		}
		
		avgRFA /= expanded.length;
		avgRBA /= expanded.length;
		
		System.out.println("Final Result:");
		System.out.println("RFA: " + avgRFA);
		System.out.println("RBA: " + avgRBA);
	}

}
