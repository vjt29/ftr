package ftr.graphics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import ftr.board.BlockNode;
import ftr.board.Board;
import ftr.graphics.Screen;

public class MazeDisplay {

	// used in calculations
	private BlockNode[][] board;
	private int screenWidth;
	private int screenHeight;
	private int cellSizePixels;

	// graphics
	private JFrame jf;
	private Screen screen;
	private JPanel centralPanel;
	private String message;

	public MazeDisplay(Board maze, int screenWidth, int screenHeight, String message) {
		// variable assignment
		this.board = maze.board;
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		this.message = message;

		// the larger dimension of width vs length for both screen and board
		int cellSizePixels = (screenWidth < screenHeight ? screenWidth : screenHeight)
				/ (board.length < board[0].length ? board.length : board[0].length);

		// readying the window
		jf = new JFrame(message);
		jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		// initialize
		screen = new Screen(board, cellSizePixels);

		// creating a layout
		centralPanel = new JPanel(new BorderLayout());
		centralPanel.add(screen, BorderLayout.CENTER);
		centralPanel.setBackground(Color.WHITE);

		// adding layout to frame
		jf.add(centralPanel, BorderLayout.CENTER);
		jf.setSize(screenWidth, screenHeight);
	}
	
	public void show(){
		jf.setVisible(true);
	}
	
	public void setPath(LinkedList<BlockNode> path){
		screen.setPath(path);
	}
	
	public void setFinalPath(LinkedList<BlockNode> finalPath){
		screen.setFinalPath(finalPath);
	}
	
	public void repaint(){
		screen.repaint();
	}

}
