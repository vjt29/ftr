package ftr.graphics;

/* Library imports */
import javax.swing.JPanel;

import ftr.board.BlockNode;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

public class Screen extends JPanel {

	// serializeable stuff
	private static final long serialVersionUID = 1L;

	// class variables
	private BlockNode[][] board;
	private final int scaleFactor;
	private final int width;
	private final int height;

	// Paths to be drawn
	LinkedList<BlockNode> path;
	LinkedList<BlockNode> finalPath;

	public Screen(BlockNode[][] board, int scaleFactor) {
		this.board = board;
		this.scaleFactor = scaleFactor;
		this.width = this.board.length * scaleFactor;
		this.height = this.board[0].length * scaleFactor;
	}

	public void setPath(LinkedList<BlockNode> path) {
		this.path = path;
	}
	
	public void setFinalPath(LinkedList<BlockNode> finalPath){
		this.finalPath = finalPath;
	}

	public void paint(Graphics g) {
		// iterate over board, color cells according to value
		Color[][] maze = buildBoard();
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				g.setColor(maze[i][j]);
				g.fillRect(i * scaleFactor, j * scaleFactor, scaleFactor, scaleFactor);
			}
		}
	}

	private Color[][] buildBoard() {
		Color[][] maze = new Color[board.length][board[0].length];
		// populating the maze with colors
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j].type == BlockNode.GOAL) {
					maze[i][j] = Color.GREEN;
				} else if (board[i][j].type == BlockNode.WALL) {
					maze[i][j] = Color.BLACK;
				} else if (board[i][j].type == BlockNode.START) {
					maze[i][j] = Color.GRAY;
				} else {
					maze[i][j] = Color.WHITE;
				}
			}
		}

		if (finalPath != null) {
			for(BlockNode bn : finalPath){
				maze[bn.x][bn.y] = Color.BLUE;
			}
		}
		
		if (path != null) {
			// iterating over the path
			for (BlockNode bn : path) {
				if (board[bn.x][bn.y].type == BlockNode.WALL) {
					maze[bn.x][bn.y] = Color.ORANGE;
				} else if (board[bn.x][bn.y].type == BlockNode.OPEN) {
					maze[bn.x][bn.y] = Color.RED;
				}
			}
		}
		
		return maze;

	}
}
