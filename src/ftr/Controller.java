package ftr;

import javafx.animation.AnimationTimer;
import javafx.collections.FXCollections;

import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import java.io.IOException;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import ftr.algorithms.AdaptiveAStar;
import ftr.algorithms.RepeatedBackwardAStar;
import ftr.algorithms.RepeatedForwardAStar;
import ftr.board.BlockNode;
import ftr.board.Board;
import ftr.comparator.FValue;
import ftr.tree.StateNode;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Controller {
	@FXML
	Canvas canvas1;
	@FXML
	Canvas canvas2;
	Board b;
	AnimationTimer timer;
	
	RepeatedForwardAStar algo;
	LinkedList<BlockNode> path;
	LinkedList<BlockNode> finalPath;
	
	Comparator<StateNode> comparator;
	
	private final ReadWriteLock lock = new ReentrantReadWriteLock();
	
	public void start() {
		comparator= new FValue();
		b = new Board(50);
		Lock w = lock.writeLock();
    	Lock r = lock.readLock();
    	w.lock();
		try {
			drawBoard(b, canvas1);
		}finally {
			w.unlock();
		}
		//rba= new RepeatedBackwardAStar(b);
		
		algo = new RepeatedForwardAStar(b, comparator, "rfa");
        
	}
	public void switchToAAS() {
		clearAlgo();
		algo = new AdaptiveAStar(b, comparator, "aas");
	}
	public void switchToRFA() {
		clearAlgo();
		algo = new RepeatedForwardAStar(b, comparator, "rfa");
	}
	
	public void switchToRBA() {
		clearAlgo();
		algo = new RepeatedBackwardAStar(b, comparator, "rba");
	}
	public void play() {
		Task<LinkedList<BlockNode>> task = new Task<LinkedList<BlockNode>>() {
		    @Override public LinkedList<BlockNode> call() throws InterruptedException {
		    	
		        algo.eval();
		        return algo.finalPath;
		    }
		    
		};

		new Thread(task).start();
		timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
            	try {
	            	Lock w = lock.writeLock();
	            	Lock r = lock.readLock();
	            	w.lock();
	        		try {
	                	path = algo.naivePath;
	                	finalPath = algo.finalPath;
	        		}finally {
	        			w.unlock();
	        		}
	            	if (algo.naiveBoard != null) {
	            		w.lock();
	            		try {
	                		drawBoard(algo.naiveBoard,canvas2);
	                		drawBoard(b, canvas1);
	            		}finally {
	            			w.unlock();
	            		}
	            	}else {
	                	//drawBoard(b, canvas2);
	            	}	
            	}catch(ConcurrentModificationException cme) {
            		try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
            	}
            }
        };
        timer.start();
	}
	public void clearAlgo() {
		timer.stop();
		Lock w = lock.writeLock();
		w.lock();
		try {
			finalPath=null;
			path=null;
			GraphicsContext gc = canvas2.getGraphicsContext2D();
			gc.clearRect(0, 0,canvas2.getWidth() , canvas2.getHeight());
			start();
		}finally {
			w.unlock();
		}

		timer.start();
		
		
		
	}
	
	
	
	public void drawBoard(Board b, Canvas canvas) {
		boolean draw=true;
		GraphicsContext gc = canvas.getGraphicsContext2D();
		double h = canvas.getHeight()/b.board.length;
		double w = canvas.getWidth()/b.board[0].length;
		
		
		for(int i = 0; i<b.board.length;i++) {
			for(int j = 0; j<b.board.length;j++) {
				if (b.board[i][j].type == BlockNode.GOAL) {
					gc.setFill(Color.GREEN);
				} else if (b.board[i][j].type == BlockNode.WALL) {
					gc.setFill(Color.BLACK);
				} else if (b.board[i][j].type == BlockNode.START) {
					gc.setFill(Color.GRAY);
				} else {
					gc.setFill(Color.WHITE);
				}
				gc.fillRect(i*w, j*h, w, h);
				
			}
		}
		if (finalPath != null) {
			for(BlockNode bn : finalPath){
				gc.setFill(Color.BLUE);
				gc.fillRect(bn.x*w, bn.y*h, w, h);
				
			}
			
		}
		
		if (path != null) {
			// iterating over the path
			for (BlockNode bn : path) {
				if (b.board[bn.x][bn.y].type == BlockNode.WALL) {
					gc.setFill(Color.ORANGE);
					gc.fillRect(bn.x*w, bn.y*h, w, h);
				} else if (b.board[bn.x][bn.y].type == BlockNode.OPEN) {
					gc.setFill(Color.RED);
					gc.fillRect(bn.x*w, bn.y*h, w, h);
				}
			}
		}
	}		
}
